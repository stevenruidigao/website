---
title: "Reflow The Logo: Logo Contest"
date: 2021-04-16T1:13:43+00:00
featureImage: images/allpost/horatii.jpg
postImage: images/allpost/socrates.jpg
---

TWO logos.  Winner for each logo will get $500.

Logo 1, is for Repair Preservation Group RPG is a nonprofit org with mission to make independent repair affordable, accessible and reliable for all people.

Logo 2, is for Fight to Repair — the legislative arm of RPG that is fighting legislative battles to further RPGs mission.

Anyone can submit logos in any format from svg vector to sketch on a napkin. Submit your entries by tweet with hashtag #reflowthelogo and follow [@fighttorepair](https://twitter.com/fighttorepair) on twitter. Submissions accepted until May 1st. Winner will be contacted by DM on Twitter.
